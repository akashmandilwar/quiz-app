import React, { Component } from 'react';
import axios from 'axios';
import { Route,Redirect } from 'react-router-dom';
import Quiz from './components/Quiz';
import Quiz1 from './components/Quiz1';
import Result from './components/Result';
class App8 extends Component {
        state={
         data:[],
         data1:[],
         name:"",wrongAttempt:0,z:0,
         date:""
        }
        async componentDidMount() {
        
            let x=await axios.get("https://aqueous-falls-84437.herokuapp.com/ques");
            let {data}=this.state;
            while(data.length<=5){
                 let y=Math.floor(Math.random() * 15);
                 if(data.findIndex(n=>n.ques===x.data[y].ques)===-1){
                     console.log(x.data[y].ques);
                     data.push(x.data[y]);
                     this.setState(data);
                 }
            }
            
    }
    handleChange1=(temp,temp1)=>{
        this.setState({name:temp,date:temp1});
    }
        handleChange=(temp)=>{
              this.setState({wrongAttempt:this.state.wrongAttempt+temp});
        }
        handleChange2=async(temp)=>{
            let x=await axios.get("https://aqueous-falls-84437.herokuapp.com/ques");
            
            let data=[];
            console.log(data);
            
            while(data.length<=5){
                 let y=Math.floor(Math.random() * 15);
                 if(data.findIndex(n=>n.ques===x.data[y].ques)===-1){
                     console.log(x.data[y].ques);
                     data.push(x.data[y]);
                     this.setState({data});
                 }
            }
            this.setState({wrongAttempt:0,date:temp});
        }
    render() { 
        let {date,wrongAttempt,name,data}=this.state;
        console.log("name=",this.state.name,"wrongAttempt=",this.state.wrongAttempt,"date",this.state.date,data);
        return ( 
            <div>
                <Route path="/" exact  render={(props)=><Quiz onOptChange={this.handleChange1} name={name} date={date} wrong={wrongAttempt} {...props} />}/>
                <Route path="/question/:id" exact   render={(props)=><Quiz1 ques={data} onOptChange={this.handleChange} name={name} date={date} wrong={wrongAttempt} {...props} />}/>
                <Route path="/result" exact  render={(props)=><Result handleChange={this.handleChange2} name={name} date={date} wrongAttempt={wrongAttempt} {...props} />}/>
            </div>
         );
    }
}
 
export default App8;