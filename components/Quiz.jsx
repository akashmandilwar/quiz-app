import React, { Component } from 'react';
import axios from 'axios';
class Quiz extends Component {
    state = { 
        questions:[],
        data:{"name":""},
        index:0,x:0,
     }
     
handleChange=e=>{
    const {currentTarget:input}=e;
    let data=this.state;
    data[input.name]=input.value;
    this.setState({data});
}
handleClick= async()=>{
    let {data}=this.state;
    var tempDate = new Date();
    var date = tempDate.getFullYear() + '-' + (tempDate.getMonth()+1) + '-' + tempDate.getDate() +' '+ tempDate.getHours()+':'+ tempDate.getMinutes()+':'+ tempDate.getSeconds();
    console.log(date);
       this.props.onOptChange(data.name,date);
    this.props.history.push({
        pathname:"/question/1"
    })
}
    render() { 
        let {data}=this.state;
        return ( 
                <div className="container">
                    <div className="row px-5">
                        <div className="col-md-6 mx-auto col-12 bg-light" style={{marginTop:"40px",minH eight:"300px"}}>
                            <div className="text-center mt-4"><h3>Welcome To the Quiz World</h3></div>
                            <div className="text-center mt-4"><label className="text-primary">For play quiz you must have to enter your name.</label></div>
                            <div className="form-group row mt-4">
                                <label className="col-form-group col-md-3 col-3">Name:</label>
                                <div className="col-md-9 col-9">
                                        <input className="form-control" value={data.name} type="text" name="name" id="name" onChange={this.handleChange}  placeholder="Enter Your Name"/>
                                </div>
                                <div className="col-md-12 col-12 text-center mt-5">
                                    <button className="btn btn-primary btn-md"  onClick={this.handleClick}>Next</button>
                                </div>
                            </div>
                                   
                            
                        </div>
                    </div>
                </div>
         );
        
    }
}
 
export default Quiz;