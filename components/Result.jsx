import React, { Component } from 'react';
import axios from 'axios';
class Result extends Component {
    state = {  }
    async componentDidMount() {
        let {name,wrongAttempt,date}=this.props;
        console.log(name,wrongAttempt,date);
        await axios.post("https://aqueous-falls-84437.herokuapp.com/detail",{
            name:name,
            wrongAttempt:wrongAttempt,
            date:date
        });
    }
    handleReplay=()=>{
        var tempDate = new Date();
        var date = tempDate.getFullYear() + '-' + (tempDate.getMonth()+1) + '-' + tempDate.getDate() +' '+ tempDate.getHours()+':'+ tempDate.getMinutes()+':'+ tempDate.getSeconds();
        this.props.handleChange(date);
        this.props.history.push({
            pathname:"/question/1"
        })
    }
    render() { 
        return ( <div className="container border bg-light">
            <h3 className="text-center">Quiz is Over!!</h3>
            <h4 className="text-center">Your Wrong Attempt:{this.props.wrongAttempt}</h4>
            <button className="btn btn-primary" onClick={this.handleReplay}>Replay</button>
        </div> );
    }
}
 
export default Result;