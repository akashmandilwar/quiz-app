import React, { Component } from 'react';
class Quiz1 extends Component {
    state = { 
           wrongattempt:0,x:0,error:""
     }
     handleClick=(temp)=>{
    this.props.history.push({
        pathname:"/question"+"/"+this.props.match.params.id+1
    })
}
handleOption=async(temp)=>{
    let {ques}=this.props;
    let x=this.props.match.params.id;
        x=+x;
        console.log(x);
    if(ques[x-1].answer===temp) {
        if(x<5){
        this.props.history.push({
            pathname:"/question"+"/"+(x+1)
        });
        this.setState({x:0,error:""});
    }
       if(x===5){
           this.props.history.push({
            pathname:"/result"
        });  
       } 
    }
    if(ques[x-1].answer!==temp) {
        this.props.onOptChange(1);
        this.setState({x:1,error:"Wrong Answer"});
        
    }
}
    render() { 
        let {error}=this.state;
        let {ques}=this.props;
        let x=this.props.match.params.id;
        return ( 
            <div style={{background:"blue",height:"500px"}} >
                <div className="container">
                    <div className="row">
                        <div className="col-3"></div>
                        <div className="col-5 bg-light ml-4 mt-4" >
                       <div>
                          <div> <label style={{fontWeight:"bold"}}>QNo.{x}:-</label></div>
                           <div className="text-center"><img className="img-responsive" style={{textlign:"center",display:"block",maxWidth:"100%",height:"auto"}} src={ques[x-1].imgLink}/></div>
                        </div> 
                        {error!==null ? <div className="text-danger"><label style={{fontWeight:"bold"}}>{error}</label> </div>:""}
                        <div className="mt-2"><label style={{fontWeight:"bold"}}>{ques[x-1].ques}</label></div>
                       <div className="border mt-1" onClick={()=>this.handleOption(ques[x-1].A)} style={{cursor:"pointer",borderRadius:"4px"}}><h6 className="ml-2">A.{ques[x-1].A}</h6></div>
                       <div className="border mt-1" onClick={()=>this.handleOption(ques[x-1].B)} style={{cursor:"pointer"}}><h6 className="ml-2">B.{ques[x-1].B}</h6></div>
                       <div className="border mt-1" onClick={()=>this.handleOption(ques[x-1].C)} style={{cursor:"pointer"}}><h6 className="ml-2">C.{ques[x-1].C}</h6></div>
                        </div>

                        <div className="col-4"></div>
                    </div>
                    
                </div>
            </div>
         );
        
    }
}
 
export default Quiz1    ;